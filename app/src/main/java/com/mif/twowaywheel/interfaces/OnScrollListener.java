package com.mif.twowaywheel.interfaces;

import com.mif.twowaywheel.view.TwoWayWheel;

/**
 * The listener interface for receiving onScroll events. The class that is interested in processing a onScroll event implements
 * this interface, and the object created with that class is registered with a component using the component's
 * <code>addOnScrollListener<code> method. When
 * the onScroll event occurs, that object's appropriate
 * method is invoked.
 *
 * @see
 */
public interface OnScrollListener {

    /**
     * On scroll started.
     *
     * @param view       the view
     * @param value      the value
     * @param roundValue the round value
     */
    void onScrollStarted(TwoWayWheel view, float value, int roundValue);

    /**
     * On scroll.
     *
     * @param view       the view
     * @param value      the value
     * @param roundValue the round value
     */
    void onScroll(TwoWayWheel view, float value, int roundValue);

    /**
     * On scroll finished.
     *
     * @param view       the view
     * @param value      the value
     * @param roundValue the round value
     */
    void onScrollFinished(TwoWayWheel view, float value, int roundValue);
}
