package com.mif.twowaywheel.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import com.mif.twowaywheel.R;
import com.mif.twowaywheel.interfaces.IFlingRunnable;
import com.mif.twowaywheel.interfaces.OnScrollListener;


public class TwoWayWheel extends View implements OnGestureListener, IFlingRunnable.FlingRunnableView {

    protected final int BITMAP_TYPE_TICK = 0;
    protected final int BITMAP_TYPE_DELIMETER = 1;

    protected final int ANIMATION_DURATION = 200;
    protected final int ALPHA_MAX = 255;

    protected final float FADE_END_PERCANTAGE = 0.3f;
    protected final float TICK_HEIGHT_KOEF = 0.33f;

    private final String TAG = TwoWayWheel.class.getSimpleName();

    protected int mColorDelimiter;
    protected int mColorTick;

    protected int mWidth;
    protected int mHeight;
    protected int mMaxScrollableWidth;
    protected int mMinScrollableWidth;

    protected Paint mPaint;

    protected Bitmap mTickBitmap;
    protected Bitmap mDelimiterBitmap;

    protected float mTickSpace = 30f;
    protected float mTickSize = 7.0f;
    protected int mOriginalDeltaX = 0;

    protected int mTicksCount;
    protected int mWheelMaxRounds = 2;
    protected int mDelimiterValue = 1;

    protected IFlingRunnable mFlingRunnable;
    protected OnScrollListener mScrollListener;

    // It can't be actually limitless, but it's limit is hardly reachable
    protected boolean mIsInfinite;

    private DrawFilter mFast;
    private DrawFilter mDF;

    private boolean mIsFirstScroll;
    private boolean mIsOnLayout = false;
    private boolean mIsForcedLayout = true;
    private boolean mIsMovingToLeft;

    private ScrollSelectionNotifier mScrollSelectionNotifier;
    private GestureDetector mGestureDetector;
    private int mTouchSlop;

    public TwoWayWheel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public TwoWayWheel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context, attrs, 0);
    }

    public TwoWayWheel(Context context) {
        this(context, null);
        init(context, null, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        mFlingRunnable = new FlingRunnable(this, ANIMATION_DURATION);

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TwoWayWheel, defStyle, 0);

            mWheelMaxRounds = a.getInteger(R.styleable.TwoWayWheel_maxSpins, 2);

            mTickSize = a.getDimension(R.styleable.TwoWayWheel_tickSize, 4f);
            mColorTick = a.getColor(R.styleable.TwoWayWheel_tickColor, Color.WHITE);
            mTickSpace = a.getDimension(R.styleable.TwoWayWheel_tickSpace, mTickSize * 3.5f);

            mDelimiterValue = a.getInt(R.styleable.TwoWayWheel_delimiterValue, 5);
            mColorDelimiter = a.getColor(R.styleable.TwoWayWheel_delimiterColor, mColorTick);

            mIsInfinite = a.getBoolean(R.styleable.TwoWayWheel_infinite, false);
            if (mIsInfinite) {
                // Let's hope there wouldn't be screens with more than 4320p(8k UHD)
                mWheelMaxRounds = Integer.MAX_VALUE / 7680;
            }

            a.recycle();
        }

        mFast = new PaintFlagsDrawFilter(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG, 0);
        mPaint = new Paint(Paint.FILTER_BITMAP_FLAG);

        mGestureDetector = new GestureDetector(context, this);
        mGestureDetector.setIsLongpressEnabled(false);
        setFocusable(true);
        setFocusableInTouchMode(true);

        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    protected Bitmap makeBitmap(int type, int viewHeight) {
        final int width = (int) Math.ceil(mTickSize);

        float ellipse = width / 2;

        Bitmap bm = Bitmap.createBitmap(width, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);

        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setDither(true);

        if (type == BITMAP_TYPE_DELIMETER) {
            p.setColor(mColorDelimiter);

        } else if (type == BITMAP_TYPE_TICK) {
            p.setColor(mColorTick);
        }

        float bottom = viewHeight - getPaddingBottom();
        float top = getPaddingTop();
        if (type == BITMAP_TYPE_DELIMETER) {
            top = getPaddingTop();

        } else if (type == BITMAP_TYPE_TICK) {
            top +=(viewHeight - getPaddingTop() - getPaddingBottom()) * TICK_HEIGHT_KOEF;
        }

        RectF rect = new RectF(0, top, width, bottom);
        c.drawRoundRect(rect, ellipse, ellipse, p);

        return bm;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.setDrawFilter(mDF);

        final int w = mWidth;
        int total = mTicksCount;

        mPaint.setShader(null);

        for (int i = 0; i < total; i++) {
            float x = (mOriginalDeltaX + (((float) i / total) * w));

            if (x < 0) {
                x = w - (-x % w);
            } else {
                x = x % w;
            }

            int alpha = (int) (ALPHA_MAX * Math.min((0.5 - Math.abs(x / w - 0.5f)) /
                    (0.5 - Math.abs(FADE_END_PERCANTAGE - 0.5f)), 1));
            mPaint.setAlpha(alpha);

            x += getPaddingLeft();

            canvas.drawBitmap((i % mDelimiterValue == 0) ? mDelimiterBitmap : mTickBitmap, x, 0, mPaint);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        mIsOnLayout = true;

        if (changed || mIsForcedLayout) {

            mWidth = right - left - getPaddingLeft() - getPaddingRight();
            int height = bottom - top;

            mTicksCount = (int) (mWidth / (mTickSize + mTickSpace));

            mMaxScrollableWidth = mWidth * mWheelMaxRounds;
            mMinScrollableWidth = -mMaxScrollableWidth;

            mTickBitmap = makeBitmap(BITMAP_TYPE_TICK, height);
            mDelimiterBitmap = makeBitmap(BITMAP_TYPE_DELIMETER, height);
        }

        mIsOnLayout = false;
        mIsForcedLayout = false;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onDown(android.view.MotionEvent)
     */
    @Override
    public boolean onDown(MotionEvent event) {
        mDF = mFast;
        mFlingRunnable.stop(false);
        mIsFirstScroll = true;
        return true;
    }

    /**
     * On up.
     */
    void onUp() {
        if (mFlingRunnable.isFinished()) {
            scrollIntoSlots();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onFling(android.view.MotionEvent, android.view.MotionEvent, float, float)
     */
    @Override
    public boolean onFling(MotionEvent event0, MotionEvent event1, float velocityX, float velocityY) {

        boolean toleft = velocityX < 0;

        if (!toleft) {
            if (mOriginalDeltaX > mMaxScrollableWidth) {
                mFlingRunnable.startUsingDistance(mOriginalDeltaX, mMaxScrollableWidth - mOriginalDeltaX);
                return true;
            }
        } else {
            if (mOriginalDeltaX < mMinScrollableWidth) {
                mFlingRunnable.startUsingDistance(mOriginalDeltaX, mMinScrollableWidth - mOriginalDeltaX);
                return true;
            }
        }

        mFlingRunnable.startUsingVelocity(mOriginalDeltaX, (int) velocityX / 2);
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onLongPress(android.view.MotionEvent)
     */
    @Override
    public void onLongPress(MotionEvent arg0) {
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onScroll(android.view.MotionEvent, android.view.MotionEvent, float, float)
     */
    @Override
    public boolean onScroll(MotionEvent event0, MotionEvent event1, float distanceX, float distanceY) {
        getParent().requestDisallowInterceptTouchEvent(true);

        if (mIsFirstScroll) {
            if (distanceX > 0) {
                distanceX -= mTouchSlop;
            } else {
                distanceX += mTouchSlop;
            }
            scrollStarted();
        }

        mIsFirstScroll = false;

        float delta = -1 * distanceX;
        mIsMovingToLeft = delta < 0;

        if (!mIsMovingToLeft) {
            if (mOriginalDeltaX + delta > mMaxScrollableWidth) {
                delta /= (((float) mOriginalDeltaX + delta) - mMaxScrollableWidth) / 10;
            }
        } else {
            if (mOriginalDeltaX + delta < mMinScrollableWidth) {
                delta /= -(((float) mOriginalDeltaX + delta) - mMinScrollableWidth) / 10;
            }
        }

        trackMotionScroll((int) (mOriginalDeltaX + delta));
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onShowPress(android.view.MotionEvent)
     */
    @Override
    public void onShowPress(MotionEvent arg0) {
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.GestureDetector.OnGestureListener#onSingleTapUp(android.view.MotionEvent)
     */
    @Override
    public boolean onSingleTapUp(MotionEvent arg0) {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.View#onTouchEvent(android.view.MotionEvent)
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean retValue = mGestureDetector.onTouchEvent(event);

        int action = event.getAction();
        if (action == MotionEvent.ACTION_UP) {
            mDF = null;
            onUp();
        } else if (action == MotionEvent.ACTION_CANCEL) {
        }
        return retValue;
    }

    /**
     * Track motion scroll.
     *
     * @param newX the new x
     */
    @Override
    public void trackMotionScroll(int newX) {
        mOriginalDeltaX = newX;
        scrollRunning();
        invalidate();
    }

    /**
     * Scroll into slots.
     */
    @Override
    public void scrollIntoSlots() {

        if (!mFlingRunnable.isFinished()) {
            return;
        }

        if (mOriginalDeltaX > mMaxScrollableWidth) {
            mFlingRunnable.startUsingDistance(mOriginalDeltaX, mMaxScrollableWidth - mOriginalDeltaX);
            return;
        } else if (mOriginalDeltaX < mMinScrollableWidth) {
            mFlingRunnable.startUsingDistance(mOriginalDeltaX, mMinScrollableWidth - mOriginalDeltaX);
            return;
        }

        float diff = Math.abs(mOriginalDeltaX % mTickSpace);
        float diff2 = (int) (mTickSpace - diff);

        if (diff > 0.1f && diff2 > 0.1f) {
            if (diff <= (mTickSpace / 2f)) {
                // Scroll back
                mFlingRunnable.startUsingDistance(mOriginalDeltaX, (int) (mIsMovingToLeft ? diff : -diff));
            } else {
                // Scroll further
                mFlingRunnable.startUsingDistance(mOriginalDeltaX, (int) (mIsMovingToLeft ? diff2 :-diff2));
            }
        } else {
            onFinishedMovement();
        }
    }

    /**
     * On finished movement.
     */
    private void onFinishedMovement() {
        scrollCompleted();
    }


    /**
     * Scroll completed.
     */
    void scrollCompleted() {
        if (mScrollListener != null) {
            if (mIsOnLayout) {
                if (mScrollSelectionNotifier == null) {
                    mScrollSelectionNotifier = new ScrollSelectionNotifier();
                }
                post(mScrollSelectionNotifier);
            } else {
                fireOnScrollCompleted();
            }
        }
    }

    /**
     * Scroll started.
     */
    void scrollStarted() {
        if (mScrollListener != null) {
            if (mIsOnLayout) {
                if (mScrollSelectionNotifier == null) {
                    mScrollSelectionNotifier = new ScrollSelectionNotifier();
                }
                post(mScrollSelectionNotifier);
            } else {
                fireOnScrollStarted();
            }
        }
    }

    /**
     * Scroll running.
     */
    void scrollRunning() {
        if (mScrollListener != null) {
            if (mIsOnLayout) {
                if (mScrollSelectionNotifier == null) {
                    mScrollSelectionNotifier = new ScrollSelectionNotifier();
                }
                post(mScrollSelectionNotifier);
            } else {
                fireOnScrollRunning();
            }
        }
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public float getValue() {
        int w = getWidth() - getPaddingLeft() - getPaddingRight();
        int position = mOriginalDeltaX;
        float value = (float) position / (w * mWheelMaxRounds);
        return value;
    }

    /**
     * Gets the tick value.
     *
     * @return the tick value
     */
    int getTickValue() {
        return (int) ((getCurrentPage() * mTicksCount) + (mOriginalDeltaX % mWidth) / mTickSpace);
    }

    /**
     * Return the total number of ticks available for scrolling.
     *
     * @return the ticks count
     */
    public int getAvarageTicksCount() {
        try {
            return (int) (((mMaxScrollableWidth / mWidth) * mTicksCount) + (mOriginalDeltaX % mWidth) / mTickSpace) * 2;
        } catch (ArithmeticException e) {
            return 0;
        }
    }

    /**
     * Gets the ticks.
     *
     * @return the ticks
     */
    public int getTicksPerRound() {
        return mTicksCount;
    }

    /**
     * Gets the current page.
     *
     * @return the current page
     */
    int getCurrentPage() {
        return (mOriginalDeltaX / mWidth);
    }

    /**
     * Fire on scroll completed.
     */
    private void fireOnScrollCompleted() {
        mScrollListener.onScrollFinished(this, getValue(), getTickValue());
    }

    /**
     * Fire on scroll started.
     */
    private void fireOnScrollStarted() {
        mScrollListener.onScrollStarted(this, getValue(), getTickValue());
    }

    /**
     * Fire on scroll running.
     */
    private void fireOnScrollRunning() {

        int value = getTickValue();

        if (null != mScrollListener) {
            mScrollListener.onScroll(this, getValue(), value);
        }
    }

    @Override
    public int getMinX() {
        return mMinScrollableWidth;
    }

    @Override
    public int getMaxX() {
        return mMaxScrollableWidth;
    }

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.mScrollListener = onScrollListener;
    }

    /**
     * The Class ScrollSelectionNotifier.
     */
    private class ScrollSelectionNotifier implements Runnable {

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            fireOnScrollCompleted();
        }
    }
}