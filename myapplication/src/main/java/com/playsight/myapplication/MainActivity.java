package com.playsight.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.mif.twowaywheel.interfaces.OnScrollListener;
import com.mif.twowaywheel.view.TwoWayWheel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView action = (TextView) findViewById(R.id.action);
        final TextView svalue = (TextView) findViewById(R.id.value);
        final TextView rounded = (TextView) findViewById(R.id.rounded);

        TwoWayWheel wheel = (TwoWayWheel) findViewById(R.id.wheel);
        wheel.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStarted(TwoWayWheel view, float value, int roundValue) {
                action.setText("started");
                svalue.setText(String.valueOf(value));
                rounded.setText(String.valueOf(roundValue));
            }

            @Override
            public void onScroll(TwoWayWheel view, float value, int roundValue) {
                action.setText("moving");
                svalue.setText(String.valueOf(value));
                rounded.setText(String.valueOf(roundValue));
            }

            @Override
            public void onScrollFinished(TwoWayWheel view, float value, int roundValue) {
                action.setText("finished");
                svalue.setText(String.valueOf(value));
                rounded.setText(String.valueOf(roundValue));
            }
        });
    }
}
